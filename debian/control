Source: python-django-otp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 libjs-mathjax,
 pybuild-plugin-pyproject,
 python3-all,
 python3-babel,
 python3-django,
 python3-freezegun,
 python3-hatchling,
 python3-qrcode,
 python3-sphinx,
Standards-Version: 4.7.0
Homepage: https://github.com/django-otp
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-otp
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-otp.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-django-otp-doc
Section: doc
Architecture: all
Depends:
 libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: pluggable framework for two-factor authentication (Documentation)
 This project makes it easy to add support for one-time passwords (OTPs) to
 Django. It can be integrated at various levels, depending on how much
 customization is required. It integrates with django.contrib.auth, although it
 is not a Django authentication backend. The primary target is developers
 wishing to incorporate OTPs into their Django projects as a form of two-factor
 authentication.
 .
 This project includes several simple OTP plugins and more are available
 separately. This package also includes an implementation of OATH HOTP and TOTP
 for convenience, as these are standard OTP algorithms used by multiple plugins.
 .
 This package contains the documentation.

Package: python3-django-otp
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-qrcode,
Suggests:
 python-django-otp-doc (= ${binary:Version}),
Description: pluggable framework for two-factor authentication (Python3 version)
 This project makes it easy to add support for one-time passwords (OTPs) to
 Django. It can be integrated at various levels, depending on how much
 customization is required. It integrates with django.contrib.auth, although it
 is not a Django authentication backend. The primary target is developers
 wishing to incorporate OTPs into their Django projects as a form of two-factor
 authentication.
 .
 This project includes several simple OTP plugins and more are available
 separately. This package also includes an implementation of OATH HOTP and TOTP
 for convenience, as these are standard OTP algorithms used by multiple plugins.
 .
 This package contains the Python 3 version of the library.
